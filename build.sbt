ThisBuild / organization := "com.example"
ThisBuild / scalaVersion := "2.13.5"

lazy val root = (project in file(".")).settings(
  name := "index-11492",
  libraryDependencies ++= Seq(
    // "core" module - IO, IOApp, schedulers
    // This pulls in the kernel and std modules automatically.
    "org.typelevel" %% "cats-effect" % "3.3.12",
    // concurrency abstractions and primitives (Concurrent, Sync, Async etc.)
    "org.typelevel" %% "cats-effect-kernel" % "3.3.12",
    // standard "effect" library (Queues, Console, Random etc.)
    "org.typelevel" %% "cats-effect-std"      % "3.3.12",
    "org.gnieh"     %% "fs2-data-csv"         % "1.1.0",
    "org.gnieh"     %% "fs2-data-csv-generic" % "1.1.0",
    "io.minio"       % "minio"                % "8.3.6",
    "io.circe"      %% "circe-core"           % "0.14.1",
    "io.circe"      %% "circe-generic-extras" % "0.14.1",
    "io.circe"      %% "circe-generic"        % "0.14.1",
    "org.http4s"    %% "http4s-blaze-client"  % "0.23.9",
    "org.http4s"    %% "http4s-circe"         % "0.23.9",
    "org.typelevel" %% "log4cats-core"        % "2.2.0",
    "org.typelevel" %% "log4cats-slf4j"       % "2.2.0",
    "org.slf4j"      % "jcl-over-slf4j"       % "1.7.35",
    "org.slf4j"      % "jul-to-slf4j"         % "1.7.35",
    "org.slf4j"      % "log4j-over-slf4j"     % "1.7.35",

// better monadic for compiler plugin as suggested by documentation
    compilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")
  )
)
