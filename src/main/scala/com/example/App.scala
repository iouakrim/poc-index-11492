package com.example

import cats.effect.IO
import cats.effect.IOApp
import com.example.minio.MinIOStorageService
import com.example.tiny.TinyUrlClient
import io.minio.MinioClient
import org.http4s.client.Client
import org.http4s.client.JavaNetClientBuilder
import org.typelevel.log4cats.SelfAwareStructuredLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger

object App extends IOApp.Simple {
  val logger: SelfAwareStructuredLogger[IO] = Slf4jLogger.getLogger[IO]

  override def run: IO[Unit] =
    for {
      minioClient <-
        IO.delay(
          MinioClient
            .builder()
            .endpoint("http://localhost:9000")
            .credentials("RV9537FPY2IO0E24LOOL", "c1DVfwAqppbZxFYFmimi95cdW7klDDDTN1wTGUKV")
            .build()
        )
      httpClient: Client[IO] = JavaNetClientBuilder[IO].create
      minoService            = new MinIOStorageService[IO](minioClient, "automatic-reports")
      tinyUrlClient          = new TinyUrlClient(httpClient)
      processor              = new Processor[IO](minoService)
      _                      = println(s"App running")
      t0                     = System.currentTimeMillis()
      _ <- processor.run()
      _  = println(s"file uploaded to minio")
      t1 = System.currentTimeMillis()
      _  = println("Elapsed time: " + (t1 - t0) + "ms")

      _ = println(s"generating minio url")
      url <- processor.generateUrl("automatic-reports", "orga1/daily-sales-userId.csv")
      t2 = System.currentTimeMillis()
      _  = println(s"MINIO URL: $url")
      _  = println("Elapsed time: " + (t2 - t1) + "ms")

      _ = println(s"generating tiny url")
      tinyUrl <- tinyUrlClient.shortenUrl(url)
      t3 = System.currentTimeMillis()
      _  = println(s"TINY URL: $tinyUrl")
      _  = println("Elapsed time: " + (t3 - t2) + "ms")
      _  = println("TOTAL Elapsed time: " + (t3 - t0) + "ms")

    } yield ()

}
