package com.example

import cats.effect.Async
import com.example.minio.Data
import com.example.minio.MinIOStorageService
import fs2.data.csv.lowlevel
import org.typelevel.log4cats.StructuredLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class Processor[F[_]: Async](
    minIOStorageService: MinIOStorageService[F]
) {
  private val logger: StructuredLogger[F] = Slf4jLogger.getLogger[F]
  private val utf8BOM                     = fs2.Stream("\ufeff").through(fs2.text.utf8.encode)

  def run(): F[Unit] = {
    val rows = Data.data
    utf8BOM
      .append(
        rows
          .evalTap(elem => logger.info(s"elem from rows: $elem"))
          .through(lowlevel.writeWithoutHeaders)
          .through(lowlevel.toRowStrings( /* separator: Char = ',', newline: String = "\n"*/ ))
          .through(fs2.text.utf8.encode)
      )
      .through(fs2.io.toInputStream)
      .evalMap[F, Unit](
        minIOStorageService.putObject(
          s"orga1/daily-sales-userId.csv",
          _,
          "text/csv"
        )
      )
      .compile
      .drain
  }

  def generateUrl(bucket: String, path: String): F[String] = minIOStorageService.getUrl(bucket, path)

}
