package com.example.tiny

import cats.Show
import cats.effect._
import cats.implicits.toShow
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import io.circe.syntax.EncoderOps
import org.http4s.AuthScheme
import org.http4s.Credentials
import org.http4s.MediaType
import org.http4s.Uri
import org.http4s.circe.CirceEntityCodec.circeEntityDecoder
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.Method.POST
import org.http4s.client.Client
import org.http4s.client.dsl.io._
import org.http4s.client.middleware.RequestLogger
import org.http4s.client.middleware.ResponseLogger
import org.http4s.headers.Accept
import org.http4s.headers.Authorization
import org.http4s.headers.`Content-Type`
import org.typelevel.log4cats.StructuredLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger

import java.util.UUID

class TinyUrlClient(client: Client[IO]) {

  val uri = Uri.unsafeFromString("https://api.tinyurl.com") / "create"
  val asMap: Map[String, String] = Map(
    "cid" -> UUID.randomUUID().toString
  )
  val apiToken = "iCE4rV2mwOK5YvOqZFkw4a09LN8nskFmVsFbttNe3pJFx3UNSZFuHp6CVPpr"

  private val logger: StructuredLogger[IO] = Slf4jLogger.getLogger[IO]

  def shortenUrl(receiptUrl: String): IO[String] =
    for {
      _ <- logger.debug(s"Building tiny url")
      request = POST(UrlToMinify(receiptUrl), uri)
                  .putHeaders(Authorization(Credentials.Token(AuthScheme.Bearer, apiToken)))
                  .putHeaders(`Content-Type`(MediaType.application.json))
                  .putHeaders(Accept(MediaType.application.json))

      result <-
        enrichClient(client)
          .fetchAs[TinyUrlResponse](request)
          .map(r => r.data.tiny_url)
      _ <- logger.debug(s"Successfully called tiny url generator")
    } yield result

  def enrichClient(originalClient: Client[IO]): Client[IO] = {
    val clientWithRequestLogger =
      RequestLogger[IO](logHeaders = true, logBody = true, logAction = Some(logger.debug(asMap)(_)))(
        originalClient
      )
    ResponseLogger[IO](
      logHeaders = true,
      logBody = true,
      logAction = Some(logger.debug(asMap)(_))
    )(clientWithRequestLogger)
  }

  final case class TinyUrlResponse(data: TinyUrlData)

  final case class TinyUrlData(tiny_url: String)

  object TinyUrlResponse {
    implicit val TinyUrlDataCodec: Codec.AsObject[TinyUrlData]         = deriveCodec[TinyUrlData]
    implicit val TinyUrlResponseCodec: Codec.AsObject[TinyUrlResponse] = deriveCodec[TinyUrlResponse]

    implicit val TinyUrlDataShow: Show[TinyUrlData]         = _.asJson.dropNullValues.noSpaces.show
    implicit val TinyUrlResponseShow: Show[TinyUrlResponse] = _.asJson.dropNullValues.noSpaces.show
  }

}
